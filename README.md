# lpqm-crx #

Voici une petite extension Chrome qui permet d'afficher les permutations des contrepèteries du site [La Poule Qui Mue](http://lapoulequimue.fr/).

*Note:* cette extension peut aussi être utilisée avec Firefox ou tout autre navigateur supportant les [UserScripts GreaseMonkey](https://fr.wikipedia.org/wiki/Greasemonkey).

Cette extension est disponible sous la license MIT, voir le fichier LICENSE.

## Comment faire? ##

### Google Chrome ###

Le plus simple est certainement de télécharger la [version préparée](https://bitbucket.org/jbl/lpqm-crx/downloads/La%20poule%20qui%20mue.crx) de l'extension et de l'installer dans Chrome.

Une fois installée, votre prochaine visite sur [La Poule Qui Mue](http://lapoulequimue.fr/) sera légèrement différente: une icone "La Poule Qui Mue" sera affichée vers chaque contrepèterie. Si vous la cliquez, les syllabes/lettres de la contrepèterie seront modifiées pour révèler le contrepet.

Si vous voulez, vous pouvez aussi cloner https://bitbucket.org/jbl/lpqm-crx/, améliorer le code et me le faire savoir ;)

### Firefox ###

* Installer l'extension [GreaseMonkey](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/).
* Télécharger le script [lapoulequimue.js](https://bitbucket.org/jbl/lpqm-crx/raw/master/lapoulequimue.user.js)
* Drag&Drop ce script dans Firefox
* Ça devrait marcher...
