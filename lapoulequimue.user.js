// ==UserScript==
// @name          La poule qui mue

// @namespace     http://lapoulequimue.fr/

// @description   Affiche les permutations

// @include       http://lapoulequimue.fr/*

// ==/UserScript==

/**
 * The MIT License (MIT)
 * Copyright (c) 2016> Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

function swap(array, i1, i2) {
    var tmp = "";
    if (array.children[i1]) {
        tmp = array.children[i1].innerHTML;
    }
    array.children[i1].innerHTML = array.children[i2].innerHTML;
    array.children[i2].innerHTML = tmp;
}

function showInversions(event) {
    var span_map = {};
    var complexesyl = []
    var entry = event.target.parentNode;
    for (var j = 0; j < entry.children.length; j++) {
        var e = entry.children[j];
        if (e.className && e.className != 'jbl_lpqm')  {
            if (!e.classList.contains('complexesyl')) {
                if (span_map[e.classList[0]]) {
                    span_map[e.classList[0]].push(j);
                } else {
                    span_map[e.classList[0]] = [j];
                }
            } else {
                var index = parseInt(e.attributes['title'].value) - 1;
                complexesyl.push(index);
            }
        }
    }
    if (complexesyl.length > 0) {
        var temp_array = []
        for(var index = 0; index < complexesyl.length; index++) {
            temp_array[index] = entry.children[complexesyl[index]].innerHTML;
        }
        for(var index = 0; index < complexesyl.length; index++) {
            entry.children[index].innerHTML = temp_array[index];
        }
    } else {
        for(var key in span_map) {
            for (var index = 0; index < span_map[key].length; index += 2) {
                swap(entry, span_map[key][index], span_map[key][index+1]);
            }
        }
    }
}

function addListeners() {
    var entries = document.getElementsByClassName('entry-title');

    for (var i = 0; i < entries.length; i++) {
        var entry = entries.item(i);
        var already_patched = entry.getElementsByClassName("jbl_lpqm");
        for (var index = 0; index < already_patched.length; index++) {
            entry.removeChild(already_patched[index]);
        }
        var imgURL;
        try {
            imgURL = chrome.extension.getURL("images/lpqm_42x42.png");
        } catch (e) {
            imgURL = "data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAACoAAAAqCAYAAADFw8lbAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AMDCjYvoN9N5gAACcNJREFUWMPtmXlwFGUaxn/f1z3dk5MjBIgKbMIRcrgGQXCNIpewHC54roJblqgIrscuKoW7uiusbKklousBKKJiEdHFglUUxGWLK3KIQQRCuOS+gjnMJJmz+9s/OsxkkpkAK1pl1b5VU6np9HQ//X7v+zzP97ZQSil+BiH5mYT+Qy+gPOWo2nLweVCek6j6ClAK3CmIlAxEQitEUhoipSNorp8eaGjz21hln0OwHhXwgrIa/iPOPELDVwG6G+FKRGbkoRfeh0huf973E+dTo6rmBNbOZQQ3L0BoOmhGrLOaAG4UtoUKetG69UfvPQ6ZkX/hgYZKFhH6qgiCPidLjYFZISejtgVJaSB0qP8OlA3SBVIDIaN/I3RkxxyMMTMvDFDlqyG4cgb2gS/ASGw4aDufjvnQvid06Qetu0BKWnQi6+ug+jCc3AHHtjp/A3Ug9XCGMZIwRkxDXlzwA4BaIXwLbgOfp9HyhaBtJgz9KySmNclUS0+swA5AyULY9s/osrFDGAMnI3NH/g9A6yrwLboH/LUOGNuChNZw1SToMRgC/kb1eD7ta0LlQdg4Fw5vigC2grgGT0HL+fX5AfUX3Y36/mjkQJvOcMPL54VNd7lAKUKhUOwTShbCtvcjjResx7x9PiK9+7kRfnDlDFTFt+EnpX02XP9CiyBNt5sP332dD96ajel2M/f5aRS01+nfoy0Vp09hmm7cbjeapkV+1Hc89Ls3Qm2uRAJLH0HVV549o/aJ7QT+NcVpFoDUi+DmOU6NxasfITh26ADDL8/BMGHA8FFs3bSeOk8NwaDN9JfmceTgPko2rOXhJ5/h0t5XYllW5AJbFsD2xQ2ZVWjZw3ANeqRlwg8Vz2l4QgGhAAx8rEWQZ0j9qw3F6AboLsmaFcuQmkAIga4LXnjqUaorq7FtKJo3h+evvCYaaN/xUF4KJ3aA1LC2L0HLG4HskBN76e3j27CPfeOAtEPQeyyk55xTPVqWFWYmTZdomobuMtA0jbraGlyGc6sJk6fiDwSaCEEIhk1zGg3AlYC16Z34NRosnguuBOdLUjsouB0s/9nJGOiRm8uZJLkTk3n+zcV8tKGM195fQUJiEgCGKfjtoF6YRgxF00wY8JjDs7aF6NAzNlBVVwm1pyPKcXFBhJjPEsePHGLqxLG4EyTBgM3dD05h4PDRZFzShX7XDOapWfMQQuBOSOLZ1xcRarzsjSPzahj5LNhBRJtOTXm4IUIBZZ3YqXwlHyjvR48r74HNyuv1tvCpV4FgUL309OOqoINUv0xHFXSQKtNAVZw+qRrHwX271dSJ41RVZYUKBIItX9fnV94v31PW0a1R14ikTHMh22WhUjtD/k2gQmDb8Y2spvPKjD/zxqznMN0SKZ3FsRUkJqXg93kx3QkE/H40XaemqorEpGQsK3R2BcsbDZqK3fWh1S9iHd/mqFBWf+hzJ9i+uNd7ecbjzP/HTEx3NBUnJMLalZ+wZcNatn1ZzJXXXseQUTdR/J/ljBvWlzeXrsEwzfiqtX81bH2PoOVHtu6Ea8R0ENLh0eC/n8Eq+ywiZyE/9P8DZDeXM03T2PXNVm4b0g/TjK3znbN6ULRyM0nJKQQCfsb/ZgDVVRV065lHqzZpTHtxHj5fjCSc2gVLHwQjKWx+ZLuuGLfORlc+D/aRkmiToJtw8AvoMbSZ6fB5vdx30yAMQ8RbO6oqTjOmMJeUVq2oKD9Jm7T2LCkuRTQQekyQALuXR0ACCIldcxLlOYUUQjTxl41Jp/nxt195Dq+3HiFEXLJKSk4lvUNH9pftpKa6ir2lu3ji979DShlf953Cj3tNiZmM7D4Qgt5GhOqF7OuaPYBSis+Wvt8CSCdqPdX4/RH+NUzJsg8Wse7zT1tupPwbwV8TJQQyvSsipYPTTK7CiQgzGfvgBmwlIPd66DYEQpElklJy+MBe9pTuIyk5dm0q20YBAoHpdkeZGMOULF9SRL9rB8cHmpYFt7wBm+YhQn5kendcAydHd73e5w64/Ha8QQuBigIJ4DIMFrw6E6NJw9q2TcAHLjdkdu2ByzDQdBeeqkqkJsMrEQwoevW7+iw6HHR2DSOfRZegucw4pkRqsHwqDJ/RzBQLYM3Kj9F1Z9n9Ppus7K4MHnEj2fkFdMrsyrHDhwgG/FzU6RecPnGUuTOnc2DfbjK757Bk/U4s2ybYVOcbR6AeljwAnfshOveCjnlhSY+2eX4P3teGInJHwtAnwB/Jqt/n5aqsthiGJOC3+fvst8i7rA9PPnQXh/aX4fN6w8ZESA23O4F3Pl3PvTcOpvJ0Bat3l5OUnNJyRvd8ButedphGahg3zEI2aH60e/KcQkgd9q6Cda9GNdPJ40dAwRVXD6JoZTFfby5maK9LKd22BW99PUoppJQIKRsoqJ6nH53IHRMm4/XCqmWLcbmMloHuXdWwYxUIMxnZtksch199DKR0OPWbxVBeFubRmqpK7nzgj9wx4WHGjxnI0qL5tGodkc5Ysa+slEEjRxMKwvaSL9G0OOcKCVUH4cT2cLdr3fpHnFzTGpVZheBOdQyzZiA+/RNfd70PV3I7uvXM48DeMu6/7fqwtzxb+Lx1tGrjbKE3rvm85dpcNiUy8tHd6IX3t+DwpY6rcBLBNS810E0I34rpTFq4DSGcDj9XkA4luan9vhpdg2/3HI1/4voXHdluUC7XVROakX+zu2o9hyHadAlvuPpnt+fJ0TnYLTipeNEh4xL2lO5AdznlrmIt+cY3YP+asLaLVhlouSPOYRcqNcxbZ4c11x+yGH5ZBjPH9jq/KZ9SZOdfxqL5r2C6G6Y4TWdUa2fBrmURnyF1jJtfizn1k/E01xzzQtSmbkBOOh8+VMgVWW2p89tn3d7X1ihuGHcP+8q2I6VEO1Nkuhu+2wcfTYayFdHTktHPIRqbknMZ5Ip2XTHvLAp3fSBkc1GbBObc1Yd3J/YlLdlAk7E137Ztxoy9BU9NFd9XVWLbNtl53QAbVv3NsXIV34JuhJfcvHU2MuPSHzAk85QTXP4X7JOlYbrQpEABJQer2H6kmnW7v+PA6VqOVgZQCtLS03h3+QbGD8kns10CV2SlMbh3N3LTDayAN9IoVgCRmoExagaibeYFGjsWzyG08xOnOxsJgRCgS4mmCZCCao8fhEayy0I3TGzLJmQp7Ma3UQo0F1rmr3ANfeJHGORWHSa042NCWxYiXO6YRd9k3txs/64CXvT8UWgFtyBjzJguCNBGfo7Qxjex9q5GBeoc/6rsJnBVhIJ0N8JMRF5yOXrhJISZ/OOOxmPX8ClUbTnK5wHPqUYvG1IRqRkIdyoiOd152RDXwf8EQP//nqlJ/Bd38F4Pr6uugwAAAABJRU5ErkJggg==";
        }
        var s = '<img class="jbl_lpqm" src="' + imgURL + '" height="42" width="42" style="cursor: pointer;" title="Cliquez pour révèler les permutations..." alt="Cliquez pour révèler les permutations...">';
        var div = document.createElement('div');
        div.innerHTML = s;
        var img = div.firstChild;
        entry.appendChild(img);
        img.addEventListener("click", showInversions);
    }
    window.setTimeout(addListeners, 3000);
}

addListeners();
